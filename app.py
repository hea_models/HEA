import streamlit as st
import numpy as np
import os
import joblib

st.set_page_config(page_title='High-entropy alloy phase prediction',
                   layout='centered', initial_sidebar_state="collapsed")

def load_model(cpt_path):
    with open(cpt_path, 'rb') as file:
        loaded_model = joblib.load(file)
    return loaded_model


def main():
    html_temp = """
    <div>
    <h2 style="color:Blue;text-align:center;"> Предсказание фазы высокоэнтропийного сплава </h2>
    </div>
    """
    st.markdown(html_temp, unsafe_allow_html=True)

    col1, col2 = st.columns([2, 2])

    with col1:
        with st.expander('Описание', expanded=True):
            st.write("""
            Высокоэнтропийные сплавы (ВЭС) являются особым типом металлических сплавов, которые представляют огромный 
            интерес как перспективные материалы благодаря их высокой твердости, износостойкости, стойкости к 
            высокотемпературному размягчению и окислению. ВЭС обычно состоят из более чем пяти металлических элементов 
            в равных или почти равных атомных соотношениях. Понимание особенностей фазообразования в ВЭС имеет большое 
            значение при разработке новых сплавов. К настоящему времени известно, что к числу наиболее важных факторов, 
            влияющих на структурно-фазовый состав ВЭС, относятся энтальпия смешения (dHmix), энтропия смешения (dSmix), 
            разница в размерах атомов (𝛿) и концентрация валентных электронов (VEC).
            """)
        with st.expander('Принцип работы', expanded=True):
            st.write('''
            Заполните все параметры, и модель машинного обучения спрогнозирует один из пяти вариантов
            фазового состава высокоэнтропийного сплава (ОЦК-сплав, ГЦК-сплав, ГПУ-сплав, аморфный сплав, 
            многофазный сплав). 
            ''')

            st.info("""
            В качестве ввода вещественных чисел можно использовать разделители в виде точки или запятой
            """, icon="ℹ️")


    with col2:
        st.subheader('Задайте параметры ВЭС')

        num_of_elem = st.selectbox('Число элементов: ', list(range(2, 10)))

        density_calc = st.number_input('Задайте плотность ВЭС (г/см3): ',
                                       min_value=1.0, max_value=25.0, format='%.2f')

        dHmix = st.number_input('Задайте энтальпию смешения ВЭС (кДж/моль): ',
                                min_value=-50.0, max_value=5.0, format='%.2f')

        dSmix = st.number_input('Задайте энтропию смешения ВЭС (Дж/(моль*К)): ',
                                min_value=5.0, max_value=20.0, format='%.2f')

        Tm = st.number_input('Задайте температура плавления ВЭС (K): ',
                             min_value=700.0, max_value=3400.0, format='%.2f')

        Atom_size_diff = st.number_input('Задайте разницу атомных размеров ВЭС (%): ',
                                         min_value=0.0, max_value=20.0, format='%.2f')

        Elect_diff = st.number_input('Задайте разницу электроотрицательностей ВЭС: ',
                                     min_value=0.0, max_value=0.5, format='%.2f')

        VEC = st.number_input('Задайте концентрацию валентных электронов ВЭС: ',
                              min_value=2., max_value=15., format='%.2f')

        feature_list = [num_of_elem, density_calc, dHmix, dSmix, Tm, Atom_size_diff, Elect_diff, VEC]
        single_pred = np.array(feature_list).reshape(1, -1)

        if st.button('Прогноз'):
            cpt_path = os.path.join('models', 'rfc_imb_model', 'rfc_imb_model.sav')
            loaded_model = load_model(cpt_path)
            prediction = loaded_model.predict(single_pred)
            lattice_dict = {
                0: 'ОЦК-сплав',
                1: 'ГЦК-сплав',
                2: 'ГПУ-сплав',
                3: 'аморфный сплав',
                4: 'многофазный сплав'
            }
            pred_lattice = lattice_dict[prediction[0]]
            col2.subheader('Результат')
            col2.success(f'Спрогнозированный тип сплава: {pred_lattice}')


if __name__ == '__main__':
    main()
